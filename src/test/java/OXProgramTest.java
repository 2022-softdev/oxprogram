/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.aketanawat.programox.OX;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Acer
 */
public class OXProgramTest {

    public OXProgramTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCheckVerticlePlayerOCol1Win() {
        char table[][] = {{'O', '-', '-'}, 
                                 {'O', '-', '-'},
                                 {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col=1;
        assertEquals(true, OX.checkVertical(table,currentPlayer,col));
    }
     @Test
    public void testCheckVerticlePlayerOCol2NoWin() {
        char table[][] = {{'-', 'O', '-'},
                                 {'-', 'O', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int col=2;
        assertEquals(false, OX.checkVertical(table,currentPlayer,col));
    }
     @Test
    public void testCheckVerticlePlayerOCol2Win() {
        char table[][] = {{'-', 'O', '-'}, 
                                 {'-', 'O', '-'}, 
                                 {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col=2;
        int row=1;
        assertEquals(true, OX.checkWin(table,currentPlayer,row,col));
    }
    @Test
     public void testCheckVerticlePlayerOCol3Win() {
        char table[][] = {{'-', '-', 'O'}, 
                                 {'-', '-', 'O'}, 
                                 {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col=3;
        int row=1;
        assertEquals(true, OX.checkWin(table,currentPlayer,row,col));
    }
     @Test
     public void testCheckVerticlePlayerOCol3NoWin() {
        char table[][] = {{'-', '-', 'O'}, 
                                 {'-', '-', '-'}, 
                                 {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col=3;
        int row=1;
        assertEquals(false, OX.checkWin(table,currentPlayer,row,col));
    }
     @Test
     public void testCheckHorizontalPlayerOrow1Win() {
        char table[][] = {{'O', 'O', 'O'}, 
                                 {'-', '-', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int col=1;
        int row=1;
        assertEquals(true, OX.checkWin(table,currentPlayer,row,col));
    }
     @Test
     public void testCheckHorizontalPlayerOrow1NoWin() {
        char table[][] = {{'-', 'O', 'O'}, 
                                 {'-', '-', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int col=1;
        int row=1;
        assertEquals(false, OX.checkWin(table,currentPlayer,row,col));
    }
     @Test
     public void testCheckHorizontalPlayerOrow2Win() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'O', 'O', 'O'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int col=1;
        int row=2;
        assertEquals(false, OX.checkWin(table,currentPlayer,row,col));
    }
     @Test
     public void testCheckHorizontalPlayerOrow3Win() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'-', '-', '-'}, 
                                 {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int col=1;
        int row=3;
        assertEquals(true, OX.checkWin(table,currentPlayer,row,col));
    }
     @Test
     public void testCheckX1PlayerOWin() {
        char table[][] = {{'O', '-', '-'}, 
                                 {'-', 'O', '-'}, 
                                 {'-', '-', 'O'}};
        char currentPlayer = 'O';
        
        assertEquals(true, OX.checkX1(table, currentPlayer));
    }
     @Test
     public void testCheckX2PlayerOWin() {
        char table[][] = {{'-', '-', 'O'}, 
                                 {'-', 'O', '-'}, 
                                 {'O', '-', '-'}};
        char currentPlayer = 'O';
        
        assertEquals(false, OX.checkX2(table, currentPlayer));
    }
     @Test
    public void testCheckVerticlePlayerXCol1Win() {
        char table[][] = {{'X', '-', '-'}, 
                                 {'X', '-', '-'},
                                 {'X', '-', '-'}};
        char currentPlayer = 'X';
        int col=1;
        assertEquals(true, OX.checkVertical(table,currentPlayer,col));
    }
     @Test
    public void testCheckVerticlePlayerXCol2NoWin() {
        char table[][] = {{'-', 'X', '-'},
                                 {'-', 'X', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'X';
        int col=2;
        assertEquals(false, OX.checkVertical(table,currentPlayer,col));
    }
     @Test
    public void testCheckVerticlePlayerXCol2Win() {
        char table[][] = {{'-', 'X', '-'}, 
                                 {'-', 'X', '-'}, 
                                 {'-', 'X', '-'}};
        char currentPlayer = 'X';
        int col=2;
        int row=1;
        assertEquals(true, OX.checkWin(table,currentPlayer,row,col));
    }
    @Test
     public void testCheckVerticlePlayerXCol3Win() {
        char table[][] = {{'-', '-', 'X'}, 
                                 {'-', '-', 'X'}, 
                                 {'-', '-', 'X'}};
        char currentPlayer = 'X';
        int col=3;
        int row=1;
        assertEquals(true, OX.checkWin(table,currentPlayer,row,col));
    }
     @Test
     public void testCheckVerticlePlayerXCol3NoWin() {
        char table[][] = {{'-', '-', 'X'}, 
                                 {'-', '-', '-'}, 
                                 {'-', '-', 'X'}};
        char currentPlayer = 'X';
        int col=3;
        int row=1;
        assertEquals(false, OX.checkWin(table,currentPlayer,row,col));
    }
     @Test
     public void testCheckHorizontalPlayerXrow1Win() {
        char table[][] = {{'X', 'X', 'X'}, 
                                 {'-', '-', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'X';
        int col=1;
        int row=1;
        assertEquals(true, OX.checkWin(table,currentPlayer,row,col));
    }
     @Test
     public void testCheckHorizontalPlayerXrow1NoWin() {
        char table[][] = {{'-', 'X', 'X'}, 
                                 {'-', '-', '-'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'O';
        int col=1;
        int row=1;
        assertEquals(false, OX.checkWin(table,currentPlayer,row,col));
    }
     @Test
     public void testCheckHorizontalPlayerXrow2Win() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'X', 'X', 'X'}, 
                                 {'-', '-', '-'}};
        char currentPlayer = 'X';
        int col=1;
        int row=2;
        assertEquals(false, OX.checkWin(table,currentPlayer,row,col));
    }
     @Test
     public void testCheckHorizontalPlayerXrow3Win() {
        char table[][] = {{'-', '-', '-'}, 
                                 {'-', '-', '-'}, 
                                 {'X', 'X', 'X'}};
        char currentPlayer = 'X';
        int col=1;
        int row=3;
        assertEquals(true, OX.checkWin(table,currentPlayer,row,col));
    }
     @Test
     public void testCheckX1PlayerXWin() {
        char table[][] = {{'X', '-', '-'}, 
                                 {'-', 'X', '-'}, 
                                 {'-', '-', 'X'}};
        char currentPlayer = 'X';
        
        assertEquals(true, OX.checkX1(table, currentPlayer));
    }
     @Test
     public void testCheckX2PlayerXWin() {
        char table[][] = {{'-', '-', 'X'}, 
                                 {'-', 'X', '-'}, 
                                 {'X', '-', '-'}};
        char currentPlayer = 'X';
        
        assertEquals(false, OX.checkX2(table, currentPlayer));
    }
     @Test
      public void testCheckDraw() {
        char table[][] = {{'O', 'O', 'X'}, 
                                 {'X', 'O', 'O'}, 
                                 {'X', 'X', 'O'}};
        char currentPlayer1 = 'X';
        char currentPlayer2 = 'O';
        
        assertEquals(false, OX.checkDraw(table));
    }
      
}

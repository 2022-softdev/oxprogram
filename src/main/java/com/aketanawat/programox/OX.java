/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.aketanawat.programox;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class OX {

    public static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    public static  char currentPlayer = 'O';
    public static  int row, col;
    static Scanner kb = new Scanner(System.in);
    static boolean finish = false;

    public static void main(String[] args) {

        showWelcome();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTable() {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.print(table[r][c]);
                System.out.print(" ");
            }
            System.out.println("");
        }
    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowCol() {
        System.out.println("Please input row , col:");
        row = kb.nextInt();
        col = kb.nextInt();
    }

    public static void process() {
        if (setTable()) {
            if (checkWin(table,currentPlayer,row,col)) {
                finish = true;
                ShowWin();
                return;
            }
            switchPlayer();
        }
    }

    public static void ShowWin() {
        showTable();
        if (checkDraw(table)) {
            System.out.println(">>>Draw<<<");
            return;
        }else{
        System.out.println(">>>" + currentPlayer + " Win<<<");
        }
    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';// switchPlayer()
        } else {
            currentPlayer = 'O';
        }
    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    public static boolean checkWin(char[][] table,char currentPlayer,int row,int col) {
        if (checkVertical(table,currentPlayer,col)) {
            return true;
        } else if (checkHorizontal(table,currentPlayer,row)) {
            return true;
        } else if (checkX(table,currentPlayer)) {
            return true;
        } else if (checkDraw(table)) {
            return true;
        }
        return false;
    }

    public static boolean checkVertical(char[][] table,char currentPlayer,int col) {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkHorizontal(char[][] table,char currentPlayer,int row) {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX(char[][] table,char currentPlayer) {
        if (checkX1(table,currentPlayer)) {
            return true;
        } else if (checkX2( table,currentPlayer)) {//Arguments
            return true;
        }
        return false;
    }

    public static boolean checkX1(char[][] table,char currentPlayer) { //11,22,33
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX2(char[][] table,char currentPlayer) { //13,22,31
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkDraw(char[][] table) {
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table.length; j++) {
                if (table[i][j] == '-') {
                    return false;
                }                                                            
            }
        }
        return true;
    }
}
